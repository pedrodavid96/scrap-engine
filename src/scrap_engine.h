#ifndef scrap_engine
#define scrap_engine

struct game_offscreen_buffer {
    void *memory;
    int width;
    int height;
    int pitch;
    int bytesPerPixel;
};

void GameUpdateAndRender(
    game_offscreen_buffer *buffer,
    int blueOffset,
    int greenOffset
);

#endif
